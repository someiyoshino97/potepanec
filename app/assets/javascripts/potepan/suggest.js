jQuery(document).ready(function () {
  $(".get_suggest").autocomplete({
    source: function (req, resp) {
      $.ajax({
        url: "/potepan/suggests",
        type: "GET",
        data: {
          keyword: req.term
        },
        dataType: "json"
      }).then(function (data) {
        resp(data);
        $(".ui-front").css('z-index', '1000');
      });
    }
  });
  $(".ui-autocomplete").hover(function () {
    $(".searchBox").addClass('open');
  });
});