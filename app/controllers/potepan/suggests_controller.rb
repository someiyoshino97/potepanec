class Potepan::SuggestsController < ApplicationController
  require "./lib/api"

  def index
    return render json: "Bad Request", status: 400 if params[:keyword].blank?
    response = Api.get_suggest(params[:keyword])
    if response.status == 200
      render json: response.body, status: 200
    else
      render json: response.header.reason_phrase, status: response.status
    end
  end
end
