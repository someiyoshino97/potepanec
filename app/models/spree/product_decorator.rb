module Spree::ProductDecorator
  def self.prepended(base)
    base.scope :related_products, -> (product) do
      includes(master: [:default_price, :images]).joins(:taxons).
        where(spree_taxons: { id: product.taxon_ids }).where.not(id: product.id).
        distinct.limit(DISPLAY_NUMBER)
    end
  end

  Spree::Product.prepend self
end
