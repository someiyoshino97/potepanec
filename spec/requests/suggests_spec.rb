require 'rails_helper'

RSpec.describe "Suggests", type: :request do
  describe "GET index" do
    let(:keyword) { "r" }
    let(:status) { 200 }
    let(:body) { ["ruby1", "ruby2", "ruby3", "ruby4", "ruby5"] }

    before do
      WebMock.enable!
      WebMock.stub_request(:get, ENV['APIURL']).
        with(query: { keyword: keyword, max_num: 5 }).
        to_return(status: status, body: body)
      get potepan_suggests_path, params: { keyword: keyword }, xhr: true
    end

    context "when it returns 200 status" do
      it { expect(response.status).to eq 200 }

      it "returns expected value" do
        expect(response.body).to eq ["ruby1", "ruby2", "ruby3", "ruby4", "ruby5"].to_json
      end
    end

    context "when keyword is blank" do
      let(:keyword) { "" }

      it { expect(response.status).to eq 400 }

      it "returns Bad Request" do
        expect(response.body).to eq "Bad Request"
      end
    end

    context "when it returns other than 200 status" do
      let(:status) { [500, "Internal Server Error"] }

      it { expect(response.status).to eq 500 }

      it "returns error message" do
        expect(response.message).to eq "Internal Server Error"
      end
    end
  end
end
