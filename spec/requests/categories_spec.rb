require 'rails_helper'

RSpec.describe "Categories", type: :request do
  describe "GET /potepan/categories/(:taxons.id)" do
    let(:taxon) { create(:taxon) }
    let(:product) { create(:product) }

    it "returns a 200 response" do
      get potepan_category_path(taxon.id)
      expect(response.status).to eq 200
    end
  end
end
