require 'rails_helper'

RSpec.describe "Products", type: :request do
  describe "GET /potepan/products/show" do
    let(:taxon) { create(:taxon) }
    let(:main_product) { create(:product, taxons: [taxon]) }

    before do
      get potepan_product_path(main_product.id)
    end

    it "returns a 200 response" do
      expect(response.status).to eq 200
    end

    it "has expected value" do
      expect(response.body).to include main_product.name
      expect(response.body).to include main_product.description
    end
  end
end
