require 'rails_helper'

RSpec.describe ApplicationHelper do
  describe "full_title(page_title)" do
    subject { full_title("individual value") }

    it { is_expected.to eq "individual value - BIGBAG Store" }

    context "page_title is nil" do
      subject { full_title(nil) }

      it { is_expected.to eq "BIGBAG Store" }
    end

    context "page_title is blank" do
      subject { full_title("") }

      it { is_expected.to eq "BIGBAG Store" }
    end
  end
end
