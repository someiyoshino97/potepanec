require 'rails_helper'

RSpec.describe "Categories", type: :system do
  describe "show page" do
    let(:taxonomy) { create(:taxonomy) }
    let(:root) { taxonomy.root }
    let(:taxon) { create(:taxon, name: "taxon", taxonomy: taxonomy, parent: root) }
    let(:child_taxon) { create(:taxon, name: "child_taxon", taxonomy: taxonomy, parent: taxon) }
    let(:product) { create(:product) }

    before do
      product.taxons << child_taxon
      visit potepan_category_path(taxon.id)
    end

    it "page title has taxon name" do
      within ".page-title" do
        expect(page).to have_content taxon.name
      end
    end

    it "breadcrumb has SHOP and taxon name" do
      within ".breadcrumb" do
        expect(page).to have_content "SHOP"
        expect(page).to have_content taxon.name
      end
    end

    it "has Categories and Brand link" do
      within ".side-nav" do
        expect(page).to have_content taxonomy.name
        expect(page).to have_link(child_taxon.name, href: potepan_category_path(child_taxon.id))
        expect(page).not_to have_link(taxon.name, href: potepan_category_path(taxon.id))
      end
    end

    it "has product value and placeholder" do
      within ".productBox" do
        expect(page).to have_content product.name
        expect(page).to have_content product.display_price
        expect(page.find('img')['src']).to have_content 'noimage/large'
      end
    end

    it 'move to product show page when click product image' do
      within ".productBox" do
        find('a', text: product.name && product.display_price).click
      end
      expect(page).to have_current_path potepan_product_path(product.id)
    end

    context "when product image is attached" do
      before do
        create(:image, viewable_id: product.master.id, viewable_type: 'Spree::Variant')
        visit potepan_category_path(taxon.id)
      end

      it "has product image" do
        within ".productBox" do
          expect(page.find('img')['src']).to have_content 'thinking-cat.jpg'
        end
      end
    end
  end
end
