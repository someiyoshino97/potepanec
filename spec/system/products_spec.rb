require 'rails_helper'

RSpec.describe "Products", type: :system do
  describe "show page" do
    let(:taxon) { create(:taxon) }
    let(:another_taxon) { create(:taxon, name: "another_taxon") }
    let(:main_product) { create(:product, taxons: [taxon, another_taxon]) }

    before { visit potepan_product_path(main_product.id) }

    it "title has main product name" do
      expect(page).to have_title "#{main_product.name} - BIGBAG Store"
    end

    it 'move to top page when click header logo image' do
      find('.navbar-brand').click
      expect(page).to have_current_path potepan_root_path
    end

    it 'move to top page when click header navbar HOME link' do
      within ".navbar-right" do
        click_link "HOME"
      end
      expect(page).to have_current_path potepan_root_path
    end

    it 'move to top page when click breadcrumb HOME link' do
      within ".breadcrumb" do
        click_link "HOME"
      end
      expect(page).to have_current_path potepan_root_path
    end

    it "has main product information" do
      within ".media-body" do
        expect(page).to have_content main_product.name
        expect(page).to have_content main_product.display_price
        expect(page).to have_content main_product.description
      end
    end

    it "move to categories page when click 一覧ページへ戻る link" do
      within ".media-body" do
        click_link "一覧ページへ戻る"
      end
      expect(page).to have_current_path potepan_category_path(taxon.id)
    end

    context "when main product doesn't have taxon" do
      let(:main_product) { create(:product) }

      before { visit potepan_product_path(main_product.id) }

      it "doesn't have 一覧ページへ戻る link" do
        within ".media-body" do
          expect(page).not_to have_link '一覧ページへ戻る'
        end
      end
    end

    context "when main product image is attached" do
      let!(:image) do
        create(:image, viewable_id: main_product.master.id, viewable_type: 'Spree::Variant')
      end

      before { visit potepan_product_path(main_product.id) }

      it "has product image" do
        expect(main_product.images.any?).to be true
        within ".item" do
          expect(page).to have_selector("img")
          expect(page.find('img')['src']).to have_content 'thinking-cat.jpg'
        end
      end
    end

    context "when main product has related product" do
      let!(:other_taxon) { create(:taxon, name: "other_taxon") }
      let!(:not_related_product) { create(:product, name: "not_related", taxons: [other_taxon]) }
      let!(:product1) { create(:product, name: "related_product1", taxons: [taxon, another_taxon]) }
      let!(:product2) { create(:product, name: "related_product2", taxons: [taxon]) }

      before { visit potepan_product_path(main_product.id) }

      it "has two related products" do
        within ".productsContent" do
          expect(page).to have_selector(".productBox", count: 2)
        end
      end

      it "related products section has products information" do
        related_products = [product1, product2]

        related_products.each_with_index do |product, i|
          within all(".productBox")[i] do
            expect(page).to have_content product.name
            expect(page).to have_content product.display_price
            expect(page.find('img')['src']).to have_content 'noimage/large'
          end
        end
      end

      it "move to product page when click related product image" do
        within first(".productBox") do
          find('a', text: product1.name && product1.display_price).click
        end
        expect(page).to have_current_path potepan_product_path(product1.id)
      end

      it "doesn't have product which is not related" do
        within ".productsContent" do
          expect(page).not_to have_content not_related_product.name
        end
      end

      it "doesn't have main product" do
        within ".productsContent" do
          expect(page).not_to have_content main_product.name
        end
      end

      it "doesn't have product which is duplicate" do
        within ".productsContent" do
          expect(page).not_to have_content(product1.name, count: 2)
        end
      end

      context "when main product has five related products" do
        let!(:product3) { create(:product, name: "related_product3", taxons: [taxon]) }
        let!(:product4) { create(:product, name: "related_product4", taxons: [another_taxon]) }
        let!(:product5) { create(:product, name: "related_product5", taxons: [another_taxon]) }

        before { visit potepan_product_path(main_product.id) }

        it "has four related products" do
          within ".productsContent" do
            expect(page).to have_selector(".productBox", count: 4)
            expect(page).not_to have_content product5.name
          end
        end

        it "related products section has products information" do
          related_products = [product1, product2, product3, product4]

          related_products.each_with_index do |product, i|
            within all(".productBox")[i] do
              expect(page).to have_content product.name
              expect(page).to have_content product.display_price
              expect(page.find('img')['src']).to have_content 'noimage/large'
            end
          end
        end
      end

      context "when product image is attached" do
        let!(:image) do
          create(:image, viewable_id: product1.master.id, viewable_type: 'Spree::Variant')
        end

        before { visit potepan_product_path(main_product.id) }

        it "has product image" do
          expect(product1.images.any?).to be true
          within first(".productBox") do
            expect(page).to have_selector("img")
            expect(page.find('img')['src']).to have_content 'thinking-cat.jpg'
          end
        end
      end
    end
  end
end
