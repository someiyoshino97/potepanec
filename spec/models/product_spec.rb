require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  describe "related_products(product)" do
    subject { Spree::Product.related_products(main_product) }

    let(:taxon) { create(:taxon) }
    let(:another_taxon) { create(:taxon, name: "another_taxon") }
    let(:main_product) { create(:product, taxons: [taxon, another_taxon]) }
    let!(:other_taxon) { create(:taxon, name: "other_taxon") }
    let!(:not_related_product) { create(:product, name: "not_related", taxons: [other_taxon]) }
    let!(:product1) { create(:product, name: "related_product1", taxons: [taxon, another_taxon]) }
    let!(:product2) { create(:product, name: "related_product2", taxons: [taxon]) }
    let!(:product3) { create(:product, name: "related_product3", taxons: [taxon]) }
    let!(:product4) { create(:product, name: "related_product4", taxons: [another_taxon]) }
    let!(:product5) { create(:product, name: "related_product5", taxons: [another_taxon]) }

    it { is_expected.to contain_exactly(product1, product2, product3, product4) }
  end
end
