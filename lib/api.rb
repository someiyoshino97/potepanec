module Api
  def self.get_suggest(keyword)
    client = HTTPClient.new
    header = { "Authorization": "Bearer #{ENV['APIKEY']}" }
    query = { "keyword": keyword, "max_num": 5 }
    client.get(ENV['APIURL'], query, header)
  end
end
